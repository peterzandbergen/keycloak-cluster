#!/bin/bash

# https://127.0.0.1:46619/api/v1/namespaces/<namespace>/services/<service>:<service port>/proxy/<path>
# http://kubernetes_master_address/api/v1/namespaces/namespace_name/services/[https:]service_name[:port_name]/proxy

https://127.0.0.1:46619/api/v1/namespaces/kube-system/services/http://traefik-ingress-service:8080/proxy