# Keycloak on k8s

## Development with k3d

Create k3s cluster in docker. This installs a load balancer and the Traefik ingress controller. All set.

## Install client tools

Install the client tools we need for this installation.

- [kubectl](https://kubectl.docs.kubernetes.io/installation/kubectl/)
- [kustomization](https://kubectl.docs.kubernetes.io/installation/kubectl/)
- [helm](https://helm.sh/docs/intro/install/)

### kubectl

### kustomize

### helm

### Self signed certificate

Create a self signed certificate to test keycloak with TLS.

```
openssl req -x509 -newkey rsa:4096 -keyout tls.key -out tls.cert -days 365 -nodes
```

### Config maps

We use environment variables in combination with configMap and secret to configure the pods. 

- **keycloak-config** for the keycloak environment variables
- **keycloak-secrets** for the keycloak username and password
- **keycloak-db-config** for the keycloak database environment variables
- **keycloak-db-secrets** for the keycloak database username and password

The *-config and *secrets maps can and probably will link to the same file, as can be seen in base/kustomization.yaml. This gives us more flexibility for reuse.

```
configMapGenerator:
  - name: keycloak-config
    behavior: merge
    envs:
      - "settings.env"
  - name: keycloak-db-config
    behavior: merge
    envs:
      - "settings.env"
```

## Kustomizations

The kustomizations we start with are for dev and for prod with for each environment two variations in the way we expose the keycloak service, with a load balancer or with an ingress controller.

We start with the 

### dev-loadbalancer

Main items to customize are:

- tls.key and tls.crt files
- settings.env
- secrets.env
- password for keycloak
- password for the database

### prod-loadbalancer

Only difference with dev-loadbalancer is namespace

## Current deployments

1S: dev-loadbalancer version with customized tls.key

**Version v0.0.4**

## Prepare for deployment

Select the version that best suites you need, loadbalancer or ingress. 

Copy the directory and modify the settings.env and secrets.env files with your setting.

Add your cert and key in the secrets directory that will work for your host.

